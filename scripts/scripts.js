var $document = $(document);
var currentItemWaitingForImage = null;

$document.on("shown.bs.modal", ".modal.video-player-modal", function (e) {
    var button = $(e.relatedTarget);
    var videoID = button.data("video-id");
    var video = $("#videosWrapper")
        .find("video#" + videoID)
        .clone();

    if (video && video.length) {
        video = video[0];
    }

    var modal = $(this);
    var modalBody = modal.find(".modal-body");
    modalBody.empty();
    modalBody.append(video);

    if (video && video.play) {
        video.play();

        $(video).on("ended", function (e) {
            modal.find(".modal-body").empty();
            modal.modal("hide");
        });
    }
});

$document.on("hide.bs.modal", ".modal.video-player-modal", function (e) {
    var modal = $(this);

    var video = modal.find("video")[0];
    if (video && video.pause) {
        video.pause();
        video.currentTime = 0;
    }

    modal.find(".modal-body").empty("");
});

function makeDraggableElements() {
    $(".shelfsContainer").dragswap({
        element: ".draggable-item",
        //overClass: "over", // class when element goes over another element
        //moveClass: "moving", // class when element is moving
        //dropClass: "drop", // the class to add when the element is dropped
        dropAnimation: true
    });
}

$document.on("click", ".print-screen", function (e) {
    html2canvas(window.document.body).then(function (canvas) {
        var canvasBlob = screensShotModule.generateCanvasBlob(canvas);

        // Generate file download
        window.saveAs(canvasBlob, "presentation_screenshot.png");
    });
});

$document.on("click", "div.shelf .controls .up", function (e) {
    $this = $(this);
    var currentShelf = $this.closest(".shelf");

    var previousShelf = currentShelf.prev(".shelf");
    debugger;
    if (previousShelf && previousShelf.length) {
        var previousShelfClone = previousShelf.clone();
        previousShelf.replaceWith(currentShelf.clone());
        currentShelf.replaceWith(previousShelfClone);
    }
});

$document.on("click", "div.shelf .controls .down", function (e) {
    $this = $(this);
    var currentShelf = $this.closest(".shelf");

    var nextShelf = currentShelf.next(".shelf");
    debugger;
    if (nextShelf && nextShelf.length) {
        var nextShelfClone = nextShelf.clone();
        nextShelf.replaceWith(currentShelf.clone());
        currentShelf.replaceWith(nextShelfClone);
    }
});

$document.on("click", "div.shelf .controls .remove", function (e) {
    $this = $(this);
    var currentShelf = $this.closest(".shelf");

    var userConfirm = confirm("Delete the selected shelf?");
    if (userConfirm) {
        currentShelf.empty().remove();
    }
});

$document.on("click", "div.shelfsContainer button.addEmptyItem", function (e) {
    $this = $(this);

    var emptyItemTemplate = $("script#emptyItemTemplate").html();
    var template = $(emptyItemTemplate)

    $this
        .closest(".shelf")
        .find(".items")
        .append(template);

    makeDraggableElements();
});

$document.on("click", ".draggable-item button.close", function (e) {
    $this = $(this);

    $this.closest(".draggable-item").remove();
});

$document.on("click", ".draggable-item button.item-image-btn", function (e) {
    $this = $(this);
    currentItemWaitingForImage = $this.closest(".draggable-item");
    if (currentItemWaitingForImage) {
        $("#file-input").trigger('click');
    } else {
        console.error("draggable item not found");
        // display error
    }
});

$document.on("change", "#file-input", function (e) {
    $this = $(this);

    var files = $this[0].files;

    // FileReader support
    if (FileReader && files && files.length) {
        var fileReader = new FileReader();
        fileReader.onload = function () {

            if (currentItemWaitingForImage) {
                currentItemWaitingForImage.find("img.item-image").attr("src", fileReader.result);
                currentItemWaitingForImage = null;
            }
        }
        fileReader.readAsDataURL(files[0]);
    } else {
        //not supported
    }
});

$document.on("click", "button.addShelf", function (e) {
    $this = $(this);

    var shelfTemplate = $("script#shelfTemplate").html();

    $(".shelfsContainer")
        .find(".shelfs")
        .append(shelfTemplate);
});

makeDraggableElements();